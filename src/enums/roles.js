const ROLES = {
  ADMIN: 'ADMIN',
  CONSUMER: 'CONSUMER',
  SUPPLIER: 'SUPPLIER',
}

export default ROLES
