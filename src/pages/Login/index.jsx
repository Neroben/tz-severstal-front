import React, { useEffect, useState } from 'react'

import { Button, TextField } from '@mui/material'

import { browserHistory } from '../../browserHistory'

import auth from '../../api/auth'

import styles from './styles.module.scss'

const Login = () => {
  const [loginForm, setLoginForm] = useState({
    login: '',
    password: '',
    role: '',
  })
  const [isValid, setIsValid] = useState(false)
  const [isReg, setIsReg] = useState(false)

  useEffect(() => {
    setIsValid(loginForm.login !== '' && loginForm.password !== '')
  }, [loginForm])

  useEffect(() => {
    setLoginForm({
      login: '',
      password: '',
      role: '',
    })
  }, [isReg])

  const loginChange = (event) => {
    setLoginForm({ ...loginForm, login: event.target.value })
  }

  const passwordChange = (event) => {
    setLoginForm({ ...loginForm, password: event.target.value })
  }

  const roleChange = (event) => {
    setLoginForm({ ...loginForm, role: event.target.value })
  }

  const Submit = () => {
    if (isValid) {
      auth.login(loginForm).then(() => {
        browserHistory.push('/')
      })
    }
  }

  const Reg = () => {
    auth.reg({ email: loginForm.login, password: loginForm.password, role: loginForm.role }).then(() => {
      auth.login({ login: loginForm.login, password: loginForm.password }).then(() => {
        browserHistory.push('/')
      })
    })
  }

  // Логин, пароль, роль
  return (
    <div className={styles.wrapper}>
      <div>
        <form>
          {isReg ? (
            <>
              <TextField value={loginForm.login} onChange={loginChange} placeholder={'Email'} />
              <TextField
                type={'password'}
                value={loginForm.password}
                onChange={passwordChange}
                placeholder={'Пароль'}
              />
              <TextField value={loginForm.role} onChange={roleChange} placeholder={'Роль'} />
              <Button onClick={Reg} variant={'contained'}>
                Зарегистрироваться
              </Button>
              <Button onClick={() => setIsReg(false)} variant={'contained'}>
                Войти
              </Button>
            </>
          ) : (
            <>
              <TextField value={loginForm.login} onChange={loginChange} placeholder={'Логин'} />
              <TextField
                type={'password'}
                value={loginForm.password}
                onChange={passwordChange}
                placeholder={'Пароль'}
              />
              <Button onClick={Submit} variant={'contained'} disabled={!isValid}>
                Войти
              </Button>
              <Button onClick={() => setIsReg(true)} variant={'contained'}>
                Регистрация
              </Button>
            </>
          )}
        </form>
      </div>
    </div>
  )
}

export default Login
