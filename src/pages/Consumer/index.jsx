import React from 'react'
import Navbar from '../../components/Navbar'
import { useParams } from 'react-router-dom'
import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from '@mui/material'
import styles from './styles.module.scss'
import { browserHistory } from '../../browserHistory'
import consumers from '../../api/consumer'

const Consumer = () => {
  const { id } = useParams()

  const [rows, setRows] = React.useState([])
  const [selectedRows, setSelectedRows] = React.useState([])

  React.useEffect(() => {
    consumers.getProducts(localStorage.getItem('access_token'), id).then((res) => {
      const filteredRes = res ? res.filter((r) => r.actualPrice) : []
      setRows(filteredRes)
      setSelectedRows(filteredRes.map((r) => ({ id: r.id, weight: 0 })))
    })
  }, [])

  const Submit = () => {
    const data = selectedRows.filter((r) => r.weight).map((r) => ({ weight: r.weight, productId: r.id }))
    consumers.sendProducts(localStorage.getItem('access_token'), data).then(() => {
      browserHistory.push('/')
    })
  }

  const handleChange = (value, id) => {
    const index = selectedRows.findIndex((r) => r.id === id)
    const newSelectedRows = [...selectedRows]
    newSelectedRows[index] = { ...newSelectedRows[index], weight: `${+value}` }
    setSelectedRows(newSelectedRows)
  }

  return (
    <div>
      <Navbar />
      <div className={styles.wrapper}>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Название</TableCell>
                <TableCell align="center">Цена</TableCell>
                <TableCell align="center">Вес</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                  <TableCell align="center">{row.name}</TableCell>
                  <TableCell align="center">{row.actualPrice}</TableCell>
                  <TableCell align="center">
                    <TextField
                      value={selectedRows.find((r) => r.id === row.id)?.weight ?? ''}
                      onChange={(e) => handleChange(e.target.value, row.id)}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        {!!rows.length && (
          <Button onClick={Submit} disabled={!selectedRows.some((r) => r.weight > 0)}>
            Принять
          </Button>
        )}
      </div>
    </div>
  )
}

export default Consumer
