import React from 'react'

import {
  Box,
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from '@mui/material'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import { DateRangePicker, LocalizationProvider } from '@mui/lab'

import styles from './styles.module.scss'
import history from '../../../../api/history'

const AdminMainPage = () => {
  const [dates, setDates] = React.useState([null, null])
  const [isValid, setIsValid] = React.useState(false)
  const [rows, setRows] = React.useState([])

  React.useEffect(() => {
    setIsValid(dates[0] !== null && dates[1] !== null)
  }, [dates])

  const Submit = () => {
    if (isValid) {
      const dateStart = dates[0].toISOString().split('.')[0]
      const dateEnd = dates[1].toISOString().split('.')[0]

      history.getHistory(localStorage.getItem('access_token'), dateStart, dateEnd).then((res) => {
        setRows(res.reduce((p, c) => p.concat(c.orderLines.map((l) => ({ ...c, ...l }))), []))
      })
    }
  }

  return (
    <div className={styles.wrapper}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DateRangePicker
          startText="Начало"
          endText="Конец"
          value={dates}
          onChange={(newValue) => {
            setDates(newValue)
          }}
          renderInput={(startProps, endProps) => (
            <React.Fragment>
              <TextField {...startProps} />
              <Box sx={{ mx: 2 }}> до </Box>
              <TextField {...endProps} />
            </React.Fragment>
          )}
        />
      </LocalizationProvider>
      <Button disabled={!isValid} onClick={Submit} variant={'contained'}>
        Найти
      </Button>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Производитель</TableCell>
              <TableCell align="center">Приёмщик</TableCell>
              <TableCell align="center">Дата</TableCell>
              <TableCell align="center">Цена</TableCell>
              <TableCell align="center">Вес</TableCell>
              <TableCell align="center">Имя продукта</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row, index) => (
              <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell align="center">{row.supplierEmail}</TableCell>
                <TableCell align="center">{row.consumerEmail}</TableCell>
                <TableCell align="center">{row.date.split('.')[0]}</TableCell>
                <TableCell align="center">{row.price}</TableCell>
                <TableCell align="center">{row.weight}</TableCell>
                <TableCell align="center">{row.productName}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default AdminMainPage
