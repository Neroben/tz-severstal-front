import React from 'react'
import suppliers from '../../../../api/supplier'
import {
  Box,
  Button,
  Modal,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from '@mui/material'
import { DateRangePicker, LocalizationProvider } from '@mui/lab'
import AdapterDateFns from '@mui/lab/AdapterDateFns'

import styles from './styles.module.scss'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
  display: 'grid',
  gridTemplateRows: 'repeat(4, min-content)',
  gap: '20px',
}

const SupplierMainPage = () => {
  const [rows, setRows] = React.useState([])
  const [openedProductId, setOpenedProductId] = React.useState(null)
  const [openedNewProductModal, setOpenedNewProductModal] = React.useState(false)
  const [newName, setNewName] = React.useState('')
  const [newPrice, setNewPrice] = React.useState(0)
  const [dates, setDates] = React.useState([null, null])

  const getProducts = () => {
    suppliers
      .getProducts(localStorage.getItem('access_token'), localStorage.getItem('user_id'))
      .then((res) => setRows(res))
  }

  React.useEffect(getProducts, [])

  const Delete = (productId) => {
    suppliers.deleteProduct(localStorage.getItem('access_token'), productId).then(getProducts)
  }

  const Edit = (productId) => {
    setOpenedProductId(productId)
    setNewPrice(rows.find((r) => r.id === productId).actualPrice ?? 0)
    setDates([null, null])
  }

  const Submit = () => {
    const dateStart = dates[0].toISOString().split('.')[0]
    const dateEnd = dates[1].toISOString().split('.')[0]

    suppliers
      .updateProduct(localStorage.getItem('access_token'), {
        productId: openedProductId,
        price: newPrice,
        dateStart,
        dateEnd,
      })
      .then(getProducts)
  }

  const DeleteActualPrice = (productId) => {
    const currentDate = new Date().toISOString().split('.')[0]

    suppliers
      .deleteActualPrice(localStorage.getItem('access_token'), {
        productId,
        date: currentDate,
      })
      .then(getProducts)
  }

  const AddProduct = () => {
    setOpenedNewProductModal(true)
  }

  const Create = () => {
    suppliers.createProduct(localStorage.getItem('access_token'), newName).then(() => {
      getProducts()
      setOpenedNewProductModal(false)
    })
  }

  return (
    <>
      {openedNewProductModal && (
        <Modal
          open={openedNewProductModal}
          onClose={() => setOpenedNewProductModal(false)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <TextField
              value={newName}
              placeholder={'Название'}
              onChange={(e) => {
                setNewName(e.target.value)
              }}
            />
            <Button variant={'contained'} onClick={Create}>
              Создать
            </Button>
          </Box>
        </Modal>
      )}
      {openedProductId && (
        <Modal
          open={openedProductId}
          onClose={() => setOpenedProductId(null)}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <h1>{rows.find((r) => r.id === openedProductId).name}</h1>
            <TextField
              value={newPrice}
              placeholder={'Цена'}
              onChange={(e) => {
                if (!isNaN(+e.target.value)) {
                  setNewPrice(+e.target.value)
                }
              }}
            />
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DateRangePicker
                startText="Начало"
                endText="Конец"
                value={dates}
                onChange={(newValue) => {
                  setDates(newValue)
                }}
                renderInput={(startProps, endProps) => (
                  <React.Fragment>
                    <TextField {...startProps} />
                    <Box sx={{ mx: 2 }}> до </Box>
                    <TextField {...endProps} />
                  </React.Fragment>
                )}
              />
            </LocalizationProvider>
            <Button variant={'contained'} onClick={Submit}>
              Редактировать
            </Button>
          </Box>
        </Modal>
      )}
      <div className={styles.wrapper}>
        <Button variant={'contained'} onClick={AddProduct}>
          Добавить продукт
        </Button>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Продукт</TableCell>
                <TableCell align="center">Цена</TableCell>
                <TableCell align="center">Удалить актуальную цену</TableCell>
                <TableCell align="center">Редактировать</TableCell>
                <TableCell align="center">Удалить</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                  <TableCell align="center">{row.name}</TableCell>
                  <TableCell align="center">{row.actualPrice}</TableCell>
                  <TableCell align="center">
                    <Button onClick={() => DeleteActualPrice(row.id)}>Удалить актуальную цену</Button>
                  </TableCell>{' '}
                  <TableCell align="center">
                    <Button onClick={() => Edit(row.id)}>Редактировать</Button>
                  </TableCell>
                  <TableCell align="center">
                    <Button onClick={() => Delete(row.id)}>Удалить</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </>
  )
}

export default SupplierMainPage
