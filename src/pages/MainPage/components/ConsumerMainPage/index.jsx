import React from 'react'
import styles from './styles.module.scss'
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material'
import { browserHistory } from '../../../../browserHistory'
import consumers from '../../../../api/consumer'

const ConsumerMainPage = () => {
  const [rows, setRows] = React.useState([])

  React.useEffect(() => {
    consumers.getConsumers(localStorage.getItem('access_token')).then((res) => setRows(res))
  }, [])

  const SelectSupplier = (id) => {
    browserHistory.push(`/consumer/${id}`)
  }

  return (
    <div className={styles.wrapper}>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Поставщик</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell onClick={() => SelectSupplier(row.id)} className={styles.line} align="center">
                  {row.email}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default ConsumerMainPage
