import React, { useEffect } from 'react'
import { Button } from '@mui/material'

import { browserHistory } from '../../browserHistory'

import AdminMainPage from './components/AdminMainPage'
import ConsumerMainPage from './components/ConsumerMainPage'
import SupplierMainPage from './components/SupplierMainPage'

import styles from './styles.module.scss'
import Navbar from '../../components/Navbar'

const MainPage = () => {
  const renderRoleThings = () => {
    const role = localStorage.getItem('role')

    switch (role) {
      case 'ADMIN':
        return AdminMainPage()
      case 'CONSUMER':
        return ConsumerMainPage()
      case 'SUPPLIER':
        return SupplierMainPage()
      default:
        browserHistory.push('/login')
    }
  }

  return (
    <div className={styles.wrapper}>
      <Navbar />
      {renderRoleThings()}
    </div>
  )
}

export default MainPage
