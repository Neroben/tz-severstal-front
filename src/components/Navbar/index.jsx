import React from 'react'
import { Button } from '@mui/material'
import { browserHistory } from '../../browserHistory'
import styles from './styles.module.scss'

const Navbar = () => {
  const Exit = () => {
    localStorage.clear()
    browserHistory.push('/login')
  }

  return (
    <nav className={styles.nav}>
      <Button onClick={Exit}>Выйти</Button>
    </nav>
  )
}

export default Navbar
