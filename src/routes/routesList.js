import { lazy } from 'react'

const Login = lazy(() => import('../pages/Login/index').then((page) => ({ default: page.default })))
const MainPage = lazy(() => import('../pages/MainPage/index').then((page) => ({ default: page.default })))
const ConsumerPage = lazy(() => import('../pages/Consumer/index').then((page) => ({ default: page.default })))

const routesList = [
  {
    title: 'ТЗ Северсталь',
    path: '/',
    exact: true,
    component: MainPage,
  },
  {
    title: 'ТЗ Северсталь',
    path: '/consumer/:id',
    exact: true,
    component: ConsumerPage,
  },
  {
    title: 'ТЗ Северсталь',
    path: '/login',
    exact: true,
    component: Login,
  },
]

export default routesList
