import React, { Suspense } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import routesList from './routesList'

const Routes = () => {
  return (
    <Suspense fallback={null}>
      <Switch>
        {routesList.map((route) => (
          <Route key={route.path} path={route.path} exact={route.exact} component={route.component} />
        ))}
      </Switch>
    </Suspense>
  )
}

export default Routes
