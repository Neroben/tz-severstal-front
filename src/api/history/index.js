import axios from 'axios'

const history = {
  getHistory: async (access_token, dateStart, dateEnd) => {
    const res = await axios
      .post(
        `${process.env.REACT_APP_URL}/api/v1/history`,
        {
          dateStart,
          dateEnd,
        },
        {
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
        }
      )
      .then(({ data }) => data)
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })

    return res
  },
}

export default history
