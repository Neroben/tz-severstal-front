import axios from 'axios'

const consumers = {
  getConsumers: async (access_token, dateStart, dateEnd) => {
    const res = await axios
      .get(`${process.env.REACT_APP_URL}/api/v1/supplier`, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      })
      .then(({ data }) => data)
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })

    return res
  },
  getProducts: async (access_token, supplierId) => {
    const res = await axios
      .get(`${process.env.REACT_APP_URL}/api/v1/product/supplier-${supplierId}`, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      })
      .then(({ data }) => data)
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })

    return res
  },
  sendProducts: async (access_token, data) => {
    const res = await axios
      .post(
        `${process.env.REACT_APP_URL}/api/v1/delivery`,
        { orderLines: data },
        {
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
        }
      )
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })

    return res
  },
}

export default consumers
