import querystring from 'querystring'
import axios from 'axios'

const auth = {
  login: async ({ login, password }) => {
    await axios
      .post(
        `${process.env.REACT_APP_URL}/oauth/token`,
        querystring.stringify({
          grant_type: 'password',
          username: login,
          password: password,
        }),
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: 'Basic dXNlcjpzZWNyZXQ=',
          },
        }
      )
      .then(({ data: { access_token, refresh_token, userAuthorities, user_id } }) => {
        localStorage.setItem('access_token', access_token)
        localStorage.setItem('refresh_token', refresh_token)
        localStorage.setItem('role', userAuthorities)
        localStorage.setItem('user_id', user_id)
      })
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })
  },
  reg: async ({ email, password, role }) => {
    await axios
      .post(
        `${process.env.REACT_APP_URL}/api/v1/user/registration`,
        {
          email,
          password,
          authority: role,
        },
        {
          headers: {
            Authorization: 'Basic dXNlcjpzZWNyZXQ=',
          },
        }
      )
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })
  },
}

export default auth
