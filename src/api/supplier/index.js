import axios from 'axios'

const suppliers = {
  getProducts: async (access_token, supplierId) => {
    const res = await axios
      .get(`${process.env.REACT_APP_URL}/api/v1/product/supplier-${supplierId}`, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      })
      .then(({ data }) => data)
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })

    return res
  },
  deleteProduct: async (access_token, productId) => {
    await axios
      .delete(`${process.env.REACT_APP_URL}/api/v1/product-mod/${productId}`, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      })
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })
  },
  updateProduct: async (access_token, data) => {
    await axios
      .post(`${process.env.REACT_APP_URL}/api/v1/product-mod/price`, data, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      })
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })
  },
  deleteActualPrice: async (access_token, data) => {
    await axios
      .delete(`${process.env.REACT_APP_URL}/api/v1/product-mod/price`, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
        data,
      })
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })
  },
  createProduct: async (access_token, name) => {
    await axios
      .post(
        `${process.env.REACT_APP_URL}/api/v1/product-mod`,
        { name },
        {
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
        }
      )
      .catch((error) => {
        console.error(`Ошибка ${error}`)
      })
  },
}

export default suppliers
