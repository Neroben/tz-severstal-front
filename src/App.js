import React from 'react'
import { Router } from 'react-router-dom'

import { browserHistory } from './browserHistory'

import Routes from './routes'

const App = () => {
  return (
    <Router history={browserHistory}>
      <Routes />
    </Router>
  )
}

export default App
